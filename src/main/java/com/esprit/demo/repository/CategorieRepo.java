package com.esprit.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.esprit.demo.entities.Categorie;

public interface CategorieRepo extends CrudRepository <Categorie, Long> {

}
