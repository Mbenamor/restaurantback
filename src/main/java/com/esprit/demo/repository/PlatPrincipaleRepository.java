package com.esprit.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.esprit.demo.entities.PlatPrincipale;

@Repository
public interface PlatPrincipaleRepository extends CrudRepository<PlatPrincipale, Long>{

}
