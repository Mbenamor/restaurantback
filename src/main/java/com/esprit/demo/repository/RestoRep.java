package com.esprit.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.esprit.demo.entities.Restaurant;

public interface RestoRep extends CrudRepository<Restaurant, Long> {

}
