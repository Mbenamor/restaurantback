package com.esprit.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.esprit.demo.entities.PlatPersonalise;

@Repository
public interface PlatPersonaliseRepo extends CrudRepository<PlatPersonalise, Long> {
	
	@Query(value="SELECT * FROM t_plat_perso u WHERE u.resto_id_res= :restaurant", nativeQuery = true)
	List<PlatPersonalise> findByIDResto(@Param("restaurant")Long idresto);

}
