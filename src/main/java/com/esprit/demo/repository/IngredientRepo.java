package com.esprit.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.esprit.demo.entities.Ingredient;

@Repository
public interface IngredientRepo  extends CrudRepository<Ingredient, Long>{

}
