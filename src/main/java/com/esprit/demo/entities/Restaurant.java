package com.esprit.demo.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "T_Resto")
public class Restaurant implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRes")
	private long idResto;
	@Column(name = "nomRes")
	private String nomResto;
	@Column(name = "adresseRes")
	private String adresseResto;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "resto")
	@JsonIgnore
	private Set<PlatPersonalise> platPersonalises;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "restaurant")
	@JsonIgnore
	private List<PlatPrincipale> platPrincipales;
	

	



	public Restaurant(long idResto, String nomResto, String adresseResto, Set<PlatPersonalise> platPersonalises) {
		super();
		this.idResto = idResto;
		this.nomResto = nomResto;
		this.adresseResto = adresseResto;
		this.platPersonalises = platPersonalises;
	}



	public Set<PlatPersonalise> getPlatPersonalises() {
		return platPersonalises;
	}



	public void setPlatPersonalises(Set<PlatPersonalise> platPersonalises) {
		this.platPersonalises = platPersonalises;
	}
	public List<PlatPrincipale> getPlatPrincipales() {
		return platPrincipales;
	}



	public void setPlatPrincipales(List<PlatPrincipale> platPrincipales) {
		this.platPrincipales = platPrincipales;
	}



	public long getIdResto() {
		return idResto;
	}

	public void setIdResto(long idResto) {
		this.idResto = idResto;
	}

	public String getNomResto() {
		return nomResto;
	}

	public void setNomResto(String nomResto) {
		this.nomResto = nomResto;
	}

	public String getAdresseResto() {
		return adresseResto;
	}

	public void setAdresseResto(String adresseResto) {
		this.adresseResto = adresseResto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Restaurant() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresseResto == null) ? 0 : adresseResto.hashCode());
		result = prime * result + (int) (idResto ^ (idResto >>> 32));
		result = prime * result + ((nomResto == null) ? 0 : nomResto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Restaurant other = (Restaurant) obj;
		if (adresseResto == null) {
			if (other.adresseResto != null)
				return false;
		} else if (!adresseResto.equals(other.adresseResto))
			return false;
		if (idResto != other.idResto)
			return false;
		if (nomResto == null) {
			if (other.nomResto != null)
				return false;
		} else if (!nomResto.equals(other.nomResto))
			return false;
		return true;
	}

	public Restaurant(long idResto, String nomResto, String adresseResto) {
		super();
		this.idResto = idResto;
		this.nomResto = nomResto;
		this.adresseResto = adresseResto;
	}

}
