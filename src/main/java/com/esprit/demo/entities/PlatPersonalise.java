package com.esprit.demo.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


import java.util.Set;

import javax.persistence.CascadeType;

@Entity
@Table(name = "T_PlatPerso")
public class PlatPersonalise implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idPlat")
	private long idPlatPerso;
	@Column(name = "nomPlat")
	private String nomPlatPerso;
	@Column(name = "descPlat")
	
	private String descrPlatPerso;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "PlatPersonalise")
	@JsonIgnore
	private Set<Ingredient> ingredients;
	@ManyToOne(cascade = CascadeType.ALL)
	private Restaurant resto;
	@ManyToOne(cascade = CascadeType.ALL)
	private Categorie categorie;

	public PlatPersonalise(long idPlatPerso, String nomPlatPerso, String descrPlatPerso, Set<Ingredient> ingredients,
			Restaurant resto, Categorie categorie) {
		super();
		this.idPlatPerso = idPlatPerso;
		this.nomPlatPerso = nomPlatPerso;
		this.descrPlatPerso = descrPlatPerso;
		this.ingredients = ingredients;
		this.resto = resto;
		this.categorie = categorie;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public long getIdPlatPerso() {
		return idPlatPerso;
	}

	public void setIdPlatPerso(long idPlatPerso) {
		this.idPlatPerso = idPlatPerso;
	}

	public String getNomPlatPerso() {
		return nomPlatPerso;
	}

	public void setNomPlatPerso(String nomPlatPerso) {
		this.nomPlatPerso = nomPlatPerso;
	}

	public String getDescrPlatPerso() {
		return descrPlatPerso;
	}

	public void setDescrPlatPerso(String descrPlatPerso) {
		this.descrPlatPerso = descrPlatPerso;
	}

	public PlatPersonalise(long idPlatPerso, String nomPlatPerso, String descrPlatPerso, Set<Ingredient> ingredients) {
		super();
		this.idPlatPerso = idPlatPerso;
		this.nomPlatPerso = nomPlatPerso;
		this.descrPlatPerso = descrPlatPerso;
		this.ingredients = ingredients;
	}

	public Set<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descrPlatPerso == null) ? 0 : descrPlatPerso.hashCode());
		result = prime * result + (int) (idPlatPerso ^ (idPlatPerso >>> 32));
		result = prime * result + ((nomPlatPerso == null) ? 0 : nomPlatPerso.hashCode());
		return result;
	}

	public PlatPersonalise(long idPlatPerso, String nomPlatPerso, String descrPlatPerso) {
		super();
		this.idPlatPerso = idPlatPerso;
		this.nomPlatPerso = nomPlatPerso;
		this.descrPlatPerso = descrPlatPerso;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlatPersonalise other = (PlatPersonalise) obj;
		if (descrPlatPerso == null) {
			if (other.descrPlatPerso != null)
				return false;
		} else if (!descrPlatPerso.equals(other.descrPlatPerso))
			return false;
		if (idPlatPerso != other.idPlatPerso)
			return false;
		if (nomPlatPerso == null) {
			if (other.nomPlatPerso != null)
				return false;
		} else if (!nomPlatPerso.equals(other.nomPlatPerso))
			return false;
		return true;
	}

	public PlatPersonalise() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Restaurant getResto() {
		return resto;
	}

	public void setResto(Restaurant resto) {
		this.resto = resto;
	}

	public PlatPersonalise(long idPlatPerso, String nomPlatPerso, String descrPlatPerso, Set<Ingredient> ingredients,
			Restaurant resto) {
		super();
		this.idPlatPerso = idPlatPerso;
		this.nomPlatPerso = nomPlatPerso;
		this.descrPlatPerso = descrPlatPerso;
		this.ingredients = ingredients;
		this.resto = resto;
	}

}
