package com.esprit.demo.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "platPrincipal")
public class PlatPrincipale implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPlat;
	private String nomPlat;
	private String description;
	// @ManyToMany(mappedBy="platPrin")
	// List<Restaurant> restaurants;
	@ManyToOne
	Restaurant restaurant;
	@ManyToOne(cascade = CascadeType.ALL)
	private Categorie categories;
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((idPlat == null) ? 0 : idPlat.hashCode());
		result = prime * result + ((nomPlat == null) ? 0 : nomPlat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlatPrincipale other = (PlatPrincipale) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idPlat == null) {
			if (other.idPlat != null)
				return false;
		} else if (!idPlat.equals(other.idPlat))
			return false;
		if (nomPlat == null) {
			if (other.nomPlat != null)
				return false;
		} else if (!nomPlat.equals(other.nomPlat))
			return false;
		return true;
	}

	@XmlAttribute(name = "id")
	public Long getIdPlat() {
		return idPlat;
	}

	public void setIdPlat(long idPlat) {
		this.idPlat = idPlat;
	}

	@XmlElement(name = "platName")
	public String getNomPlat() {
		return nomPlat;
	}

	public void setIdPlat(Long idPlat) {
		this.idPlat = idPlat;
	}

	public void setNomPlat(String nomPlat) {
		this.nomPlat = nomPlat;
	}

	@XmlElement(name = "platDesc")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement(name = "restaurant")
	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
	@XmlElement(name = "categorie")
	public Categorie getCategories() {
		return categories;
	}

	public void setCategories(Categorie categories) {
		this.categories = categories;
	}

	public PlatPrincipale(Long idPlat, String nomPlat, String description) {
		super();
		this.idPlat = idPlat;
		this.nomPlat = nomPlat;
		this.description = description;
	}

	public PlatPrincipale() {
		super();
		// TODO Auto-generated constructor stub
	}

}
