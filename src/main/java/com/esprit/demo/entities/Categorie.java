package com.esprit.demo.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "T_Categorie")
public class Categorie implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCat")
	private long idCat;
	@Column(name = "nomCat")
	private String nomCat;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "categorie")
	@JsonIgnore
	private Set<PlatPersonalise> platPerso;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "categories")
	@JsonIgnore
	private Set<PlatPrincipale> platPrin;

	public Categorie(long idCat, String nomCat, Set<PlatPersonalise> platPerso) {
		super();
		this.idCat = idCat;
		this.nomCat = nomCat;
		this.platPerso = platPerso;
	}

	public Set<PlatPersonalise> getPlatPerso() {
		return platPerso;
	}

	public void setPlatPerso(Set<PlatPersonalise> platPerso) {
		this.platPerso = platPerso;
	}

	public Set<PlatPrincipale> getPlatPrin() {
		return platPrin;
	}

	public void setPlatPrin(Set<PlatPrincipale> platPrin) {
		this.platPrin = platPrin;
	}

	public long getIdCat() {
		return idCat;
	}

	public void setIdCat(long idCat) {
		this.idCat = idCat;
	}

	public String getNomCat() {
		return nomCat;
	}

	public void setNomCat(String nomCat) {
		this.nomCat = nomCat;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Categorie() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idCat ^ (idCat >>> 32));
		result = prime * result + ((nomCat == null) ? 0 : nomCat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categorie other = (Categorie) obj;
		if (idCat != other.idCat)
			return false;
		if (nomCat == null) {
			if (other.nomCat != null)
				return false;
		} else if (!nomCat.equals(other.nomCat))
			return false;
		return true;
	}

	public Categorie(long idCat, String nomCat) {
		super();
		this.idCat = idCat;
		this.nomCat = nomCat;
	}

}
