package com.esprit.demo.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Ingredients implements Serializable {
private static final long serialVersionUID = 1L;
	

private List<Ingredient> ingred= new ArrayList<>();

public List<Ingredient> getIngred() {
	return ingred;
}

public void setIngred(List<Ingredient> ingred) {
	this.ingred = ingred;
}

}
