package com.esprit.demo.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "T_Ingredient")
public class Ingredient implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idIngredient")
	private Long id;

	@Column(name = "ingredname")
	private String name;
	@Column(name = "ingredprice")
	private Double price;
	
	@Column(name = "ingredquantity")
	private Double quantity;
	
	@Column(name = "reference")
	private String reference;
	@ManyToOne(cascade = CascadeType.ALL)
	private PlatPersonalise PlatPersonalise;

	public PlatPersonalise getPlatPersonalises() {
		return PlatPersonalise;
	}

	public void setPlatPersonalise(PlatPersonalise platPersonalise) {
		PlatPersonalise = platPersonalise;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public PlatPersonalise getPlatPersonalise() {
		return PlatPersonalise;
	}

	public Ingredient(Long id, String name, Double price, Double quantity, String reference,
			com.esprit.demo.entities.PlatPersonalise platPersonalise) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.reference = reference;
		PlatPersonalise = platPersonalise;
	}

	public Ingredient(String name, Double price, Double quantity) {
		super();
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public Ingredient() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ingredient(Long id, String name, Double price, Double quantity) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public Ingredient(Long id, String name, Double price, Double quantity,
			com.esprit.demo.entities.PlatPersonalise platPersonalise) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		PlatPersonalise = platPersonalise;
	}

}
