package com.esprit.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.demo.entities.Categorie;
import com.esprit.demo.entities.PlatPersonalise;
import com.esprit.demo.repository.CategorieRepo;
import com.esprit.demo.repository.PlatPersonaliseRepo;

@Service
public class PlatPersonaliseService implements PlatPersonaliseInterface {

	@Autowired
	PlatPersonaliseRepo rPlatPersonalise;
	@Autowired
	CategorieRepo catR;

	@Override
	public List<PlatPersonalise> getAllPlatPersonalise() {
		return (List<PlatPersonalise>) rPlatPersonalise.findAll();
		// TODO Auto-generated method stub

	}

	@Override
	public Boolean addPlatPersonalise(PlatPersonalise platP) {
		// TODO Auto-generated method stub
		if (rPlatPersonalise.save(platP) != null)
			return true;
		else
			return false;
	}

	@Override
	public void updatePlatPersonalise(PlatPersonalise platP) {
		// TODO Auto-generated method stub
		rPlatPersonalise.save(platP);
	}

	@Override
	public void deletePlatPersonalise(PlatPersonalise platP) {
		// TODO Auto-generated method stub
		rPlatPersonalise.delete(platP);
	}

	@Override
	public PlatPersonalise getPlatPersonaliseById(Long idPlat) {
		// TODO Auto-generated method stub
		return rPlatPersonalise.findById(idPlat).get();
	}

	@Override
	public void assignPlatToCat(Long idPlat, Long idCat) {
		// TODO Auto-generated method stub
		PlatPersonalise p = rPlatPersonalise.findById(idPlat).get();
		Categorie c = catR.findById(idCat).get();
		p.setCategorie(c);
		rPlatPersonalise.save(p);

	}

	@Override
	public List<PlatPersonalise> findByIDReso(Long id) {
			return (List<PlatPersonalise>) rPlatPersonalise.findByIDResto(id);
		
	}

}
