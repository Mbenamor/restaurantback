package com.esprit.demo.service;

import java.util.List;

import com.esprit.demo.entities.PlatPersonalise;

public interface PlatPersonaliseInterface {
	public List<PlatPersonalise> getAllPlatPersonalise();
	public Boolean addPlatPersonalise(PlatPersonalise platP);
	public void updatePlatPersonalise (PlatPersonalise platP);
	public void deletePlatPersonalise (PlatPersonalise platP);
	public PlatPersonalise getPlatPersonaliseById(Long idPlat);
	public void assignPlatToCat(Long idPlat, Long idCat);
	
	public List<PlatPersonalise> findByIDReso(Long id);
}
