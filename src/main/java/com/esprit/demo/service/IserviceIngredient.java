package com.esprit.demo.service;

import java.util.List;

import com.esprit.demo.entities.Ingredient;



public interface IserviceIngredient {
	public List<Ingredient> getAllIngredients();
    public Ingredient getIngredientById(Long id);
    public void deleteIngredient(Long id);
    public String saveAndUpdate(Ingredient ingredient);
    public void affecterIng_PlatPerso(Long idIng, Long IdPlatP);
	void affecterIngredientPlatPerso(int platId, int ingId);
 
}
