package com.esprit.demo.service;

import java.util.List;

import com.esprit.demo.entities.Categorie;

public interface IserviceCategorie {
	public List<Categorie> getAllCategorie();

	public Boolean addCategorie(Categorie Cat);

	public void deleteCategorie(Categorie Cat);

	public Categorie getCategorieById(long idCat);
}
