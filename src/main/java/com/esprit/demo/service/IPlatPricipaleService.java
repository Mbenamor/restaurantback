package com.esprit.demo.service;

import java.util.List;


import com.esprit.demo.entities.PlatPrincipale;

public interface IPlatPricipaleService {
	public List<PlatPrincipale> getAllPlatPrincipale();
	public Boolean addPlatPrincipale(PlatPrincipale platP);
	public PlatPrincipale updatePlatPrincipale (PlatPrincipale platP);
	public void deletePlatPrincipale (PlatPrincipale platP);
	public PlatPrincipale getPlatPrincipaleById(Long idPlat);
	public void ajouterEtaffecterListePlat(Long idPlat, Long idRes);
	public void assignPlatPrinToCat(Long idPlat, Long idCat);
}
