package com.esprit.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.demo.entities.Categorie;
import com.esprit.demo.repository.CategorieRepo;

@Service
public class CategorieService implements IserviceCategorie {

	@Autowired
	CategorieRepo catR;
	
	@Override
	public List<Categorie> getAllCategorie() {
		// TODO Auto-generated method stub
		return (List<Categorie>) catR.findAll();
	}

	@Override
	public Boolean addCategorie(Categorie Cat) {
		// TODO Auto-generated method stub
		if (catR.save(Cat) != null)	
			return true;
		else
			return false;
	}

	@Override
	public void deleteCategorie(Categorie Cat) {
		// TODO Auto-generated method stub
		catR.delete(Cat);
	}

	@Override
	public Categorie getCategorieById(long idCat) {
		// TODO Auto-generated method stub
		return catR.findById(idCat).get();
	}

}
