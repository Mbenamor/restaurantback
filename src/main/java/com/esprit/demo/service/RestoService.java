package com.esprit.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.demo.entities.Ingredient;
import com.esprit.demo.entities.PlatPersonalise;
import com.esprit.demo.entities.Restaurant;
import com.esprit.demo.repository.PlatPersonaliseRepo;
import com.esprit.demo.repository.PlatPrincipaleRepository;
import com.esprit.demo.repository.RestoRep;

@Service
public class RestoService implements Iserviceresto {

	@Autowired
	RestoRep restoR;
	@Autowired
	PlatPersonaliseRepo platPersoAgent;
	@Autowired
	PlatPrincipaleRepository platPrRepo;
	
	@Override
	public List<Restaurant> getAllResto() {
		return (List<Restaurant>) restoR.findAll();
		// TODO Auto-generated method stub
		
	}

	@Override
	public Boolean addResto(Restaurant resto) {
		// TODO Auto-generated method stub
		if (restoR.save(resto) != null)	
			return true;
		else
			return false;
	}



	@Override
	public void updateResto(Restaurant resto) {
		
		// TODO Auto-generated method stub
		restoR.save(resto);
	}

	@Override
	public void deleteResto(Restaurant resto) {
		// TODO Auto-generated method stub
		restoR.delete(resto);
	}

	@Override
	public Restaurant getRestaurantById(long idRes) {
		// TODO Auto-generated method stub
		return restoR.findById(idRes).get();
	}

	@Override
	public void affecterPlatPersoResto(int platId, int restoId) {
		// TODO Auto-generated method stub
		PlatPersonalise p = platPersoAgent.findById((long) platId).get();
		Restaurant resto = restoR.findById((long) restoId).get();
		p.setResto(resto);
		platPersoAgent.save(p);
		
		
	}

}
