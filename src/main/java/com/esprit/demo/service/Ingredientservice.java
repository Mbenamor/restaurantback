package com.esprit.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.demo.entities.Ingredient;
import com.esprit.demo.entities.PlatPersonalise;
import com.esprit.demo.repository.IngredientRepo;
import com.esprit.demo.repository.PlatPersonaliseRepo;

@Service
public class Ingredientservice implements IserviceIngredient {
	@Autowired
	IngredientRepo ingredientAgent;
	@Autowired
	PlatPersonaliseRepo platPersoAgent;

	@Override
	public List<Ingredient> getAllIngredients() {
		return (List<Ingredient>) ingredientAgent.findAll();
	}

	@Override
	public Ingredient getIngredientById(Long id) {
		return ingredientAgent.findById(id).get();
	}

	@Override
	public void deleteIngredient(Long id) {
		ingredientAgent.deleteById(id);

	}

	@Override
	public String saveAndUpdate(Ingredient ingredient) {
		if (ingredientAgent.save(ingredient) != null)
			return " add ingredient success";
		else
			return " ingredient not aded";

	}

	@Override
	public void affecterIng_PlatPerso(Long idIng, Long IdPlatP) {
		// TODO Auto-generated method stub
		PlatPersonalise p = platPersoAgent.findById(IdPlatP).get();
		Ingredient i = ingredientAgent.findById(idIng).get();
		p.getIngredients().add(i);
		platPersoAgent.save(p);
	}

	@Override
	public void affecterIngredientPlatPerso(int platId, int ingId) {
		Ingredient ing = ingredientAgent.findById((long) ingId).get();
		PlatPersonalise platPerso = platPersoAgent.findById((long) platId).get();
		ing.setPlatPersonalise(platPerso);
		ingredientAgent.save(ing);
	}

}
