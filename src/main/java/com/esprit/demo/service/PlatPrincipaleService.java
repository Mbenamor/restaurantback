package com.esprit.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.demo.entities.Categorie;
import com.esprit.demo.entities.PlatPrincipale;
import com.esprit.demo.entities.Restaurant;
import com.esprit.demo.repository.CategorieRepo;
import com.esprit.demo.repository.PlatPrincipaleRepository;
import com.esprit.demo.repository.RestoRep;

@Service
public class PlatPrincipaleService implements IPlatPricipaleService {
	@Autowired
	PlatPrincipaleRepository platRepo;
	@Autowired
	RestoRep resRepo;
	@Autowired
	CategorieRepo catRepo;

	@Override
	public List<PlatPrincipale> getAllPlatPrincipale() {
		// TODO Auto-generated method stub
		return (List<PlatPrincipale>) platRepo.findAll();
	}

	@Override
	public Boolean addPlatPrincipale(PlatPrincipale platP) {
		// TODO Auto-generated method stub
		if (platRepo.save(platP) != null)
			return true;
		else
			return false;
	}

	@Override
	public PlatPrincipale updatePlatPrincipale(PlatPrincipale platP) {
		// TODO Auto-generated method stub
		platRepo.save(platP);
		return platP;

	}

	@Override
	public void deletePlatPrincipale(PlatPrincipale platP) {
		// TODO Auto-generated method stub
		platRepo.delete(platP);

	}

	@Override
	public PlatPrincipale getPlatPrincipaleById(Long idPlat) {
		// TODO Auto-generated method stub

		return platRepo.findById(idPlat).get();
	}

	@Override
	public void ajouterEtaffecterListePlat(Long idPlat, Long idRes) {
		// TODO Auto-generated method stub
		Restaurant restaurant = resRepo.findById(idRes).get();
		PlatPrincipale p = platRepo.findById(idPlat).get();		
		p.setRestaurant(restaurant);
		platRepo.save(p);		

	}

	@Override
	public void assignPlatPrinToCat(Long idPlat, Long idCat) {
		// TODO Auto-generated method stub
		PlatPrincipale p = platRepo.findById(idPlat).get();
		Categorie c = catRepo.findById(idCat).get();
		p.setCategories(c);
		platRepo.save(p);

	}

}
