package com.esprit.demo.service;

import java.util.List;

import com.esprit.demo.entities.Restaurant;

public interface Iserviceresto {
	public List<Restaurant> getAllResto();

	public Boolean addResto(Restaurant resto);

	public void updateResto(Restaurant resto);

	public void deleteResto(Restaurant resto);

	public Restaurant getRestaurantById(long idRes);
	
	void affecterPlatPersoResto(int platId, int restoId);
}
