/*package com.esprit.demo.resources;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.esprit.demo.entities.PlatPersonalise;

@Path("platPerso")
public class GestionPlatPersonalise {

	static List<PlatPersonalise> plats = new ArrayList<PlatPersonalise>();
	
	
	@POST
	@Consumes("application/xml")
	@Produces(MediaType.TEXT_PLAIN)
	public String addPlat(PlatPersonalise plat) {
		if (plats.add(plat))
			return "plat est ajouter correctement";
		else
			return "plat n'est pas ajouter";
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<PlatPersonalise> afficherListPlat() {
		if (plats == null)
			return null;
		else
			return plats;
	}

	public int getIndexById(long idPlat) {
		for (PlatPersonalise p : plats) {
			if (p.getIdPlat_perso() == idPlat)
				return plats.indexOf(p);
		}
		return -1;
	}

	@GET
	@Path("{idPlat}")
	@Produces(MediaType.APPLICATION_XML)
	public PlatPersonalise getPlatById(@PathParam(value = "idPlat") long idPlat) {
		int index = getIndexById(idPlat);
		if (index != -1) {
			return plats.get(index);
		}
		return null;
	}

	@DELETE
	@Path("{idPlat}")
	@Produces(MediaType.TEXT_PLAIN)
	public String supprimerPlat(@PathParam(value = "idPlat") long idPlat) {
		int index = getIndexById(idPlat);
		if (index != -1) {
			plats.remove(index);
			return "plat supprimer avec succes";
		}
		return "on peut pas supprimer ce plat";
	}

	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response updatePlat(PlatPersonalise plat) {
		int index = getIndexById(Integer.parseInt(plat.getDescrPlat_perso()));
		if (index != -1) {
			plats.set(index, plat);
			return Response.status(Response.Status.CREATED).entity("Plat est modifer").build();
		} else
			return Response.status(Response.Status.NOT_FOUND).build();

	}

}*/
