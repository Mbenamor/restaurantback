package com.esprit.demo.resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.esprit.demo.entities.PlatPrincipale;

@Path("platPrin")
public class GestionPlatPrincipale {

	public static List<PlatPrincipale> platPrincipales = new ArrayList<PlatPrincipale>();

	@POST
	@Path("add")
	@Consumes("application/xml")
	public Response addRestaurant(PlatPrincipale platPrin) {
		platPrincipales.add(platPrin);
		return Response.status(Response.Status.OK).entity(platPrin).build();
	}

	@GET
	@Path("getAll")
	@Produces("application/xml")
	public List<PlatPrincipale> getAllPlatPrincipale() {
		return platPrincipales;
	}

	@PUT
	@Path("update")
	@Consumes("application/xml")
	public Response updateRestaurant(PlatPrincipale platPrin) {
		int index = platPrincipales.indexOf(platPrin);
		if (index != -1) {
			platPrincipales.set(index, platPrin);
			return Response.status(Response.Status.OK).entity(platPrin).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@GET
	@Path("getPlatPrinByPlatId/{id}")
	@Produces("application/xml")
	public Response getPlatPrincipale(@PathParam("id") Long platPrinId) {
		PlatPrincipale platPrin = getPlatPrinByPlatId(platPrinId);
		if (platPrin == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.status(Response.Status.OK).entity(platPrin).build();
	}

	public PlatPrincipale getPlatPrinByPlatId(Long platPrinId) {
		List<PlatPrincipale> platPrincipaleList = platPrincipales.stream().filter(r -> r.getIdPlat() == platPrinId)
				.collect(Collectors.toList());
		if (platPrincipaleList.isEmpty()) {
			return null;
		}
		return platPrincipaleList.get(0);
	}

	@DELETE
	@Path("deletePlatPrinByPlatId/{id}")
	public Response deletePlatPrinByPlatId(@PathParam("id") Long platId) {
		Iterator<PlatPrincipale> it = platPrincipales.iterator();
		while (it.hasNext()) {
			it.next().getIdPlat().equals(platId);
			it.remove();
			return Response.status(Response.Status.OK).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
