package com.esprit.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import Exception.ResourceNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import com.esprit.demo.entities.PlatPersonalise;
import com.esprit.demo.entities.Restaurant;
import com.esprit.demo.repository.PlatPersonaliseRepo;
import com.esprit.demo.repository.RestoRep;
import com.esprit.demo.service.Iserviceresto;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;


@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class RestoController {
	@Autowired
	RestoRep restoAgent;
	@Autowired
	PlatPersonaliseRepo platPersoRepo;
	@Autowired
	Iserviceresto restoServ;

	// http://localhost:8085/SpringMVC/servlet/api/v1/Resto
   //Works :)
	@GetMapping(path = "/Resto")
	@Produces("application/json")

	public List<Restaurant> getAllResto() {
		return (List<Restaurant>) restoAgent.findAll();
	}

	// http://localhost:8085/SpringMVC/servlet/api/v1/Resto/
	//Works
	@PostMapping(path = "/Resto/")
	@Produces("application/json")
	@Consumes("application/json")
	public String addResto(@RequestBody Restaurant resto) {
		if (restoAgent.save(resto) != null)

			return "Resto est ajouter correctement";

		else
			return "Resto n'est pas ajouter";
	}
	
	
	

	// http://localhost:8085/SpringMVC/servlet/api/v1/Resto/id
	//works
	@GetMapping(path = "/Resto/{id}")
	@Produces("application/json")
	public Restaurant getRestaurantById(@PathVariable("id") Long idRes) {
		return restoAgent.findById(idRes).get();
	}

	// http://localhost:8085/SpringMVC/servlet/api/v1/Resto/id
	// Works:(
	@PutMapping(path = "/Resto/{id}")
	@Consumes("application/json")
	public ResponseEntity<Restaurant> updateResto(@RequestBody Restaurant resto,@PathVariable("id") long id) {
		
		Optional<Restaurant> Resto = restoAgent.findById(id);
		
		if (Resto.isPresent()) {
			Restaurant R = Resto.get();
			R.setNomResto(resto.getNomResto());
			R.setAdresseResto(resto.getAdresseResto());
		      return new ResponseEntity<>(restoAgent.save(R), HttpStatus.OK);
		    }else {
			      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		    }
		
	}
	
	
	
	
	
	// http://localhost:8085/SpringMVC/servlet/api/v1/Resto/id
	//works :)
	@DeleteMapping(path = "/Resto/{id}")
	@Produces("application/json")
	public String deleteResto(@PathVariable("id") Long idRes) throws ResourceNotFoundException {
		Restaurant R = restoAgent.findById(idRes).get();
		if (idRes != -1 || R != null) {
			restoAgent.delete(R);
			return "Resto deleted ";
		} else
			return "Resto undeleted";

	}
	
	

	@PostMapping("/affecterPlatRest/{idplat}/{idresto}")
	//not well
	public void affecterPlatPerso(@PathParam("idplat") int idPlat, @PathParam("idresto") int idResto) {
		restoServ.affecterPlatPersoResto(idPlat, idResto);
	}
	
	@GetMapping(path = "/Resto/platByReso/{idResto}")
	
	public List<PlatPersonalise> GetPlatByResto(@PathVariable("idResto") Long idresto) {
		System.out.println("sqdqsdqsd"+idresto+"\n");
		
		return platPersoRepo.findByIDResto(idresto);
	}
}
