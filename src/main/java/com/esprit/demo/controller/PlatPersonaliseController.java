package com.esprit.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.esprit.demo.entities.PlatPersonalise;
import com.esprit.demo.repository.PlatPersonaliseRepo;
import com.esprit.demo.service.Ingredientservice;
import com.esprit.demo.service.PlatPersonaliseInterface;

import Exception.ResourceNotFoundException;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@RestController
@RequestMapping("/api/v1")
public class PlatPersonaliseController {
	@Autowired
	PlatPersonaliseRepo agents;
	@Autowired
	Ingredientservice ingServ;
	@Autowired
	PlatPersonaliseInterface pAgents;

	// http://localhost:8080/SpringMVC/servlet/api/v1/platperso
	@GetMapping(path = "/platperso", produces = { MediaType.APPLICATION_XML_VALUE })
	public List<PlatPersonalise> getAllPlat() {
		return (List<PlatPersonalise>) agents.findAll();
	}

	@GetMapping(path = "/platperso/{id}", produces = { MediaType.APPLICATION_XML_VALUE })
	public PlatPersonalise getPlatById(@PathVariable("id") Long idPlat) {
		return agents.findById(idPlat).get();
	}

	@PostMapping(path = "/platperso", consumes = "application/xml", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String addPlat(@RequestBody PlatPersonalise platP) {
		if (agents.save(platP) != null)

			return "Plat est ajouter correctement";

		else
			return "palt n'est pas ajouter";
	}

	@PutMapping(path = "/platperso", consumes = "application/xml")
	public Response updatePlatPersonalise(@RequestBody PlatPersonalise platP) {
		if (agents.findById(platP.getIdPlatPerso()) != null) {
			agents.save(platP);
			return Response.status(Response.Status.CREATED).entity("Plat est modifer").build();
		} else
			return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DeleteMapping(path = "/platperso/{id}", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String deletePlat(@PathVariable("id") Long idPlat) throws ResourceNotFoundException {
		PlatPersonalise p = agents.findById(idPlat).get();
		if (idPlat != -1 || p != null) {
			agents.delete(p);
			return "plat deleted ";
		} else
			return "plat undeleted";

	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/affecterIngredientPlatPerso/{id_ingredient}/{id_plat}
	@PostMapping("/affecterIngredientPlatPerso/{id_ingredient}/{id_plat}")
	public void affecterIngredientPlatPerso(@PathVariable("id_plat") int platId,
			@PathVariable("id_ingredient") int ingId) {
		ingServ.affecterIngredientPlatPerso(platId, ingId);
	}
	
	// http://localhost:8080/SpringMVC/servlet/api/v1/categorie/{idPlat}/{idCat}
	@PutMapping(path = "categorie/{idPlat}/{idCat}")
	@ResponseBody
	public void affecterPlatToCat(@PathParam("idPlat") Long idPlat,
			@PathParam("idCat") Long idCat) {
		pAgents.assignPlatToCat(idPlat, idCat);
		
	}

}
