package com.esprit.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import com.esprit.demo.entities.Categorie;
import com.esprit.demo.repository.CategorieRepo;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import Exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/v1")
public class CategorieController {
	@Autowired
	CategorieRepo catAgent;
	// http://localhost:8080/SpringMVC/servlet/api/v1/Categ

	@GetMapping(path = "/Categ", produces = { MediaType.APPLICATION_XML_VALUE })
	public List<Categorie> getAllCategorie() {
		return (List<Categorie>) catAgent.findAll();
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/Categ/
	@PostMapping(path = "/Categ/", consumes = "application/xml", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String addCategorie(@RequestBody Categorie Cat) {
		if (catAgent.save(Cat) != null)

			return "Categorie est ajouter correctement";

		else
			return "Categorie n'est pas ajouter";
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/Categ/id
	@DeleteMapping(path = "/Categ/{id}", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String deleteCategorie(@PathVariable("id") Long idCat) throws ResourceNotFoundException {
		Categorie C = catAgent.findById(idCat).get();
		if (idCat != -1 || C != null) {
			catAgent.delete(C);
			return "Categorie deleted ";
		} else
			return "Categorie undeleted";
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/Categ/id
	@GetMapping(path = "/Categ/{id}", produces = "application/xml")
	public Categorie getCategorieById(@PathVariable("id") Long idCat) {
		return catAgent.findById(idCat).get();
	}
}
