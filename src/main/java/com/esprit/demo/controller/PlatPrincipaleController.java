package com.esprit.demo.controller;

import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.esprit.demo.entities.PlatPrincipale;
import com.esprit.demo.repository.PlatPrincipaleRepository;
import com.esprit.demo.service.PlatPrincipaleService;
import Exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/v1")
public class PlatPrincipaleController {
	@Autowired
	PlatPrincipaleService platPr;
	@Autowired
	PlatPrincipaleRepository platRep;

	// http://localhost:8085/SpringMVC/servlet/api/v1/platPrin
	@GetMapping(path = "/platPrin", produces = { MediaType.APPLICATION_XML_VALUE })
	public List<PlatPrincipale> getAllPlat() {
		return (List<PlatPrincipale>) platPr.getAllPlatPrincipale();
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/platPrin/{id}
	@GetMapping(path = "/platPrin/{id}", produces = { MediaType.APPLICATION_XML_VALUE })
	public PlatPrincipale getPlatById(@PathVariable("id") Long idPlat) {
		return platPr.getPlatPrincipaleById(idPlat);
	}

	// http://localhost:8085/SpringMVC/servlet/api/v1/platPrin
	@PostMapping(path = "/platPrin", consumes = "application/xml", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String addPlat(@RequestBody PlatPrincipale platP) {
		if (platPr.addPlatPrincipale(platP) != null)

			return "Plat est ajouter correctement";

		else
			return "Plat n'est pas ajouter";
	}

	// http://localhost:8085/SpringMVC/servlet/api/v1/updatePlatPrin
	@PutMapping(path = "/updatePlatPrin", consumes = "application/xml")
	public Response updatePlatPersonalise(@RequestBody PlatPrincipale platP) {

		if (platRep.findById(platP.getIdPlat()) != null) {
			platPr.updatePlatPrincipale(platP);
			return Response.status(Response.Status.CREATED).entity("Plat est modifer").build();
		} else
			return Response.status(Response.Status.NOT_FOUND).build();
	}

	// http://localhost:8085/SpringMVC/servlet/api/v1/platPrin/{id}
	@DeleteMapping(path = "/platPrin/{id}", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String deletePlat(@PathVariable("id") Long idPlat) throws ResourceNotFoundException {
		PlatPrincipale p = platPr.getPlatPrincipaleById(idPlat);
		if (idPlat != -1 || p != null) {
			platPr.deletePlatPrincipale(p);
			return "plat deleted ";
		} else
			return "plat undeleted";

	}

	// http://localhost:8085/SpringMVC/servlet/api/v1/ajouterEtaffecterListePlat/{idPlat}/{idRes}
	@PostMapping(path = "/ajouterEtaffecterListePlat/{idPlat}/{idRes}")
	@ResponseBody
	public void ajouterEtaffecterListePlat(@PathVariable("idPlat") Long idPlat, @PathVariable("idRes") Long idRes) {
		platPr.ajouterEtaffecterListePlat(idPlat, idRes);

	}

	// http://localhost:8085/SpringMVC/servlet/api/v1/assignPlatPrinToCat/{idPlat}/{idCat}
	@PutMapping(path = "assignPlatPrinToCat/{idPlat}/{idCat}")
	@ResponseBody
	public void affecterPlatToCat(@PathVariable("idPlat") Long idPlat, @PathVariable("idCat") Long idCat) {
		platPr.assignPlatPrinToCat(idPlat, idCat);

	}

}
