package com.esprit.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.demo.entities.Ingredient;
import com.esprit.demo.repository.IngredientRepo;
import com.esprit.demo.repository.PlatPersonaliseRepo;
import com.esprit.demo.service.Ingredientservice;

import Exception.ResourceNotFoundException;

import org.springframework.http.MediaType;

@RestController
@RequestMapping("/api/v1")
public class IngredientController {
	@Autowired
	IngredientRepo ingredientAgent;
	@Autowired
	PlatPersonaliseRepo platPersAgent;
	@Autowired
	Ingredientservice iServiceAgents;

	// http://localhost:8080/SpringMVC/servlet/api/v1/ingredients
	@GetMapping(path = "/ingredients", produces = { MediaType.APPLICATION_XML_VALUE })
	public List<Ingredient> getAllIngredients() {
		List<Ingredient> ingredients = (List<Ingredient>) ingredientAgent.findAll();
		return ingredients;
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/ingredients/id
	@GetMapping(path = "/ingredients/{id}", produces = "application/xml")
	public Ingredient getIngredientById(@PathVariable("id") Long id) {
		Ingredient ingredient = ingredientAgent.findById(id).get();
		return ingredient;
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/ingredients/
	@PostMapping(path = "/ingredients", consumes = "application/xml", produces = { MediaType.TEXT_PLAIN_VALUE })
	public String addIngredient(@RequestBody Ingredient ingredient) {
		if (ingredientAgent.save(ingredient) != null)
			return "add succes";
		else
			return "add unsucces";
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/ingredients/
	@DeleteMapping(path = "/ingredients/{id}")
	public Map<String, Boolean> deleteIngredient(@PathVariable("id") Long id) throws ResourceNotFoundException {
		Ingredient ingredient = ingredientAgent.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Recipe not found for this id ::" + id));
		ingredientAgent.delete(ingredient);
		Map<String, Boolean> response = new HashMap<>();
		response.put("Recipe deleted", Boolean.TRUE);
		return response;
	}

	// http://localhost:8080/SpringMVC/servlet/api/v1/ingredients/
	@PutMapping(path = "ingredients/{idIngred}/{idPlatPerso}")
	@ResponseBody
	public void affecterIngredToPlatPerso(@PathParam("idIngred") Long idIngred,
			@PathParam("idPlatPerso") Long idPlatPerso) {

		iServiceAgents.affecterIng_PlatPerso(idIngred, idPlatPerso);
	}
	@PutMapping(path = "/ingredients", consumes = "application/json")
	public String updateIngredient(@RequestBody Ingredient ing) {
		if (ingredientAgent.findById(ing.getId()) != null) {
			ingredientAgent.save(ing);
			return "Plat est modifer";
		} else
			return "not found";
	}

}
